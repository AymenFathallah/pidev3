package com.esprit.PI.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.esprit.PI.entities.Reclamation;
import com.esprit.PI.entities.Type;

@Repository
public interface ReclamationRepository extends CrudRepository<Reclamation, Integer>{

	@Query("SELECT count(*) FROM Reclamation")
    public int getNombreReclamation();
	
	@Query("SELECT count(*) FROM Reclamation  where treated ='1' ")
    public int getNombreReclamationTreated();
	
	@Query("SELECT count(*) FROM Reclamation  where treated ='0' ")
    public int getNombreReclamationNotTreated();
    @Query("SELECT id_reclamation FROM Reclamation  where id_user=:userId")
    public List<String> findAllReclamationByUser(@Param("userId")int userId);
    @Query("SELECT id_reclamation , objet FROM Reclamation  where id_user=:userId and treated ='1'")
    public List<String> findAllReclamationTreatedByUser(@Param("userId")int userId);
    @Query("SELECT id_reclamation , objet , contenu  FROM Reclamation  where id_user=:userId and type =:typeRec")
    public List<String> findAllReclamationTreatedByUserByType(@Param("userId")int userId,@Param("typeRec")Type typeRec);
    @Query("SELECT id_reclamation , contenu   FROM Reclamation  where objet=:objetRec and type =:typeRec and treated ='1'")
    public List<String> findAllReclamationSame(@Param("objetRec")String objetRec,@Param("typeRec")Type typeRec);
    @Query("SELECT id_reclamation FROM Reclamation ")
    public List<String> findAllReclamation();
    @Modifying
    @Transactional
    @Query("DELETE from Reclamation where id_reclamation=:recId ")
    public void deleteReclamation(@Param("recId")int recId);
    
 
}
