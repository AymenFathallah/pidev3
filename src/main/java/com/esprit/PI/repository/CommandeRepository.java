package com.esprit.PI.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.esprit.PI.entities.Commande;
import com.esprit.PI.entities.Ouvrage;

public interface CommandeRepository extends CrudRepository<Commande, Integer>{
	
	
	@Query("SELECT count(*) FROM Commande")
	//SELECT count(*) FROM commande
    public int getNombreCommande();
	
	@Query("SELECT id_commande,valide from Commande")
	//SELECT * FROM commande
    public List<String> getAllCommande();
	
	@Modifying
    @Transactional
	@Query("UPDATE Commande c SET c.valide=:valide1  where c.id_commande=:CommandeId")
	public void updateCommandeById (@Param("CommandeId") int CommandeId, @Param("valide1") boolean valide	);
	
	@Modifying
    @Transactional
  //DELETE from Ouvrage where id_commande=:commandeId
    @Query("DELETE from Commande c where c.id_commande = :commandeId ")
    public void deleteCommandeById(@Param("commandeId")int commandeId);

}
