package com.esprit.PI.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.esprit.PI.entities.Ouvrage;

public interface OuvrageRepository extends CrudRepository<Ouvrage, Integer>{
	
	@Query("SELECT count(*) FROM Ouvrage")
    public int getNombreOuvrage();
	
	@Query("Select id_ouvrage,titre,genre,emprunt,nb_emprunt,ref from Ouvrage")
    public List<String> getAllOuvrages();
	
	@Modifying
    @Transactional
	@Query("UPDATE Ouvrage o SET o.titre=:titre1 , o.genre=:genre1 ,o.emprunt=:emprunt1, o.ref=:ref1 where o.id_ouvrage=:ouvrageId")
	public void updateOuvrageById(@Param("titre1") String titre,
			@Param("genre1") String genre, @Param("emprunt1") boolean emprunt, @Param("ref1") String ref, @Param("ouvrageId") int ouvrageId);
	
	@Modifying
    @Transactional
    @Query("DELETE from Ouvrage where id_ouvrage=:ouvrageId ")
    public void deleteOuvrageById(@Param("ouvrageId")int ouvrageId);

}
