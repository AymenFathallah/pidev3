package com.esprit.PI.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.esprit.PI.entities.Wishlist;

public interface WishlistRepository extends CrudRepository<Wishlist, Integer>{

	@Query("SELECT count(*) FROM Wishlist")
    public int getNombreWishlist();
	
	@Query("Select id_wishlist,nom_wishlist,date_creation,description from Wishlist")
    public List<String> getAllWishlist();
	
	@Modifying
    @Transactional
    @Query("DELETE from Wishlist where id_wishlist=:id_wishlist ")
    public void deleteWishlistById(@Param("id_wishlist")int id_wishlist);
	
	@Query("Select wishlist_id from Wishlist_ouvrage where ouvrage_id=:ouvrage_id")
    public List<String> getAllWishlistWithOuvrage(@Param("ouvrage_id")Integer ouvrage_id);
	
	@Query("Select ouvrage_id from Wishlist_ouvrage where wishlist_id=:wishlist_id")
    public List<String> getOuvrageByWishlist(@Param("wishlist_id")Integer wishlist_id);
	
	@Query("SELECT id_wishlist,nom_wishlist,date_creation,description FROM Wishlist  where id_user=:id_user")
    public List<String> getAllWishlistByUser(@Param("id_user")int id_user);
	
	@Query("SELECT id_ouvrage,titre,genre FROM Ouvrage WHERE id_ouvrage NOT IN(select ouvrage_id from Wishlist_ouvrage)")
    public List<String> getOuvrageWithNoWishlist();
	
	@Query("SELECT id_ouvrage,titre,genre FROM Ouvrage WHERE id_ouvrage IN(select ouvrage_id from Wishlist_ouvrage)")
    public List<String> getOuvrageWithWishlist();
}
