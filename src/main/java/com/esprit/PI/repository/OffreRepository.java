package com.esprit.PI.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.esprit.PI.entities.Offre;

public interface OffreRepository extends CrudRepository<Offre, Integer>{

	@Query("SELECT count(*) FROM Offre")
    public int getNombreOffre();
	
	@Query("Select id_offre,nom_offre,description,pourcentage from Offre")
    public List<String> getAllOffre();
	
	@Modifying
    @Transactional
    @Query("DELETE from Offre where id_offre=:id_offre ")
    public void deleteOffreById(@Param("id_offre")int id_offre);
	
	@Query("Select offre_id from Offre_ouvrage where ouvrage_id=:ouvrage_id")
    public List<String> getAllOffreWithOuvrage(@Param("ouvrage_id")Integer ouvrage_id);
	
	@Query("Select ouvrage_id from Offre_ouvrage where offre_id=:offre_id")
    public List<String> getOuvrageByOffre(@Param("offre_id")Integer offre_id);
	
	@Query("SELECT id_offre,nom_offre,description,pourcentage FROM Offre  where id_user=:id_user")
    public List<String> getAllOffreByUser(@Param("id_user")int id_user);
	
	@Query("SELECT id_ouvrage,titre,genre FROM Ouvrage WHERE id_ouvrage NOT IN(select ouvrage_id from Offre_ouvrage)")
    public List<String> getOuvrageWithNoOffre();
	
	@Query("SELECT id_ouvrage,titre,genre FROM Ouvrage WHERE id_ouvrage IN(select ouvrage_id from Offre_ouvrage)")
    public List<String> getOuvrageWithOffre(); 
}
