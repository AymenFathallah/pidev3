package com.esprit.PI.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.esprit.PI.entities.Livre;
import com.esprit.PI.entities.Ouvrage;
import com.esprit.PI.entities.User;

@Repository

public interface StockRepository extends CrudRepository<Ouvrage, Integer>{

	@Query("SELECT stock  from Ouvrage where titre=:titre")
    public int rechercheStock(@Param("titre")String titre);
	
	@Query("SELECT count(*) from Ouvrage where titre =:titre" )			
	public int ajoutStock(@Param("titre")String titre);
	
	@Modifying
    @Transactional
    @Query("UPDATE Ouvrage o SET o.stock=:stock1 where o.id_ouvrage=:ouvrageId")
    public void deleteStock(@Param("ouvrageId")int ouvrageId , @Param("stock1")int stock1);
	

}
