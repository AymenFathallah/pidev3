package com.esprit.PI.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.esprit.PI.entities.User;


@Repository

public interface UserRepository extends CrudRepository<User, Integer>{

	@Query("SELECT count(*) FROM User")
    public int getNombreUser();
	
 
    
    @Query("Select id_user,nom_prenom,emprunt from User")
    public List<String> getAllUsers();
    
    @Modifying
    @Transactional
	@Query("UPDATE User u SET u.nom_prenom=:nom_prenom1 , u.email=:email1 ,u.emprunt=:emprunt1 where u.id_user=:userId")
	public void updateUserById(@Param("nom_prenom1") String nom_prenom,
			@Param("email1") String email, @Param("emprunt1") boolean emprunt, @Param("userId") int userId);
    
    @Modifying
    @Transactional
    @Query("DELETE from User where id_user=:userId ")
    public void deleteUserById(@Param("userId")int userId);
    
 
	
    		
}
