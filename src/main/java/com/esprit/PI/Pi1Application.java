package com.esprit.PI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan
@Configuration
@EnableJpaRepositories("com.esprit.PI.repository")
public class Pi1Application {

	public static void main(String[] args) {
		SpringApplication.run(Pi1Application.class, args);
	}

}
