package com.esprit.PI.services;

import java.util.List;

import com.esprit.PI.entities.Commande;
import com.esprit.PI.entities.Ouvrage;

public interface ICommandeService {
	
	public int addCommande(Commande commande);
	public void deleteCommandeById(int CommandeId);
	public int getNombreCommande();
	public List<String> getAllCommande();
	public void updateCommandeById(int CommandeId, boolean valide);

}
