package com.esprit.PI.services;

import java.util.List;

import com.esprit.PI.entities.Reclamation;
import com.esprit.PI.entities.Type;



public interface IReclamationService {
	public int addReclamation(Reclamation reclamation);
	public void deleteReclamation(int reclamationId); 
	public void AffectUserToReclamation(int userId, int reclamationId);
	public List<String> findAllReclamation();
	public int getNombreReclamation();
	public int getNombreReclamationTreated();
	public int getNombreReclamationNotTreated();
	public List<String> findAllReclamationByUser(int userId);
	public List<String> findAllReclamationTreatedByUser(int userId);
	public List<String> findAllReclamationTreatedByUserByType(int userId , Type typeRec);
	public List<String> findAllReclamationSame(String objetRec, Type typeRec);

}
