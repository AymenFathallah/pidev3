package com.esprit.PI.services;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import com.esprit.PI.entities.Offre;
import com.esprit.PI.entities.Ouvrage;
import com.esprit.PI.entities.User;
import com.esprit.PI.repository.UserRepository;
import com.esprit.PI.repository.OffreRepository;

@Service
@ComponentScan
@EnableJpaRepositories
public class OffreServiceImpl implements IOffreService{

	@Autowired
	OffreRepository offreRepository;
	
	@Autowired
    OuvrageServiceImpl ouvrageService;
	
	@Autowired
	UserRepository userRepository;
	
	@Override
	public int addOffre(Offre offre) {
		
		for(Ouvrage s : offre.getOuvrages()) {
			
			System.out.println(s.getId_ouvrage());
		}
		offreRepository.save(offre);
		return offre.getId_offre();

	}

	@Override
	public void deleteOffreById(int id_offre) {
		Offre offre = offreRepository.findById(id_offre).get();
		offreRepository.delete(offre);
		
	}

	@Override
	public int getNombreOffre() {
		return offreRepository.getNombreOffre();
	}

	@Override
	public List<String> getAllOffre() {
		return offreRepository.getAllOffre();
	}

	@Override
	public List<String> getAllOffreWithOuvrage(Integer ouvrage_id) {
		return offreRepository.getAllOffreWithOuvrage(ouvrage_id);
	}

	@Override
	public void AffectUserToOffre(int userId, int id_offre) { 
		User user = userRepository.findById(userId).get();
		Offre wish = offreRepository.findById(id_offre).get();
		wish.setUser(user);
		offreRepository.save(wish);
		
	}

	@Override
	public List<String> getOuvrageByOffre(Integer offre_id) {
		return offreRepository.getOuvrageByOffre(offre_id);
	}

	@Override
	public List<String> getAllOffreByUser(int id_user) {
		return offreRepository.getAllOffreByUser(id_user);
	}
	
	@Override
	public List<String> getOuvrageWithNoOffre(){
		return offreRepository.getOuvrageWithNoOffre();
	}
	
	@Override
	public List<String> getOuvrageWithOffre(){
		return offreRepository.getOuvrageWithOffre();
	}
}
