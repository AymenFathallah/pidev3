package com.esprit.PI.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import com.esprit.PI.entities.Ouvrage;
import com.esprit.PI.repository.OuvrageRepository;
import com.esprit.PI.repository.StockRepository;

@Service
@ComponentScan
@EnableJpaRepositories
public class StockServiceImp  implements StockService {
	
	@Autowired
	 OuvrageRepository ouvrageRepository;
	
	
	@Autowired
	StockRepository stockRepository;
	
	
	@Override
	public int ajoutStock(Ouvrage ouvrage) {
		
		stockRepository.ajoutStock(ouvrage.getTitre());
		if (stockRepository.ajoutStock(ouvrage.getTitre()) == 0) {
			stockRepository.save(ouvrage);
			
		}
		else {
			System.out.println("Ce ouvrage existe deja et le stock est modifié'" );
		}
		return ouvrage.getId_ouvrage();

	}


	//@Override
	public int rechercheStock(String titre) {
		return stockRepository.rechercheStock(titre);
		
	}


	@Override
	public void deleteStock(int ouvrageId) {
		// TODO Auto-generated method stub
		
	}


}
