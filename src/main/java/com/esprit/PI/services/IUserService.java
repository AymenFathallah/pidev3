package com.esprit.PI.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.esprit.PI.entities.User;

public interface IUserService {
	public int addUser(User user);
	public void deleteUserById(int userId);
	public int getNombreUser();
	public void updateUserById(String nom_prenom,String email,boolean emprunt,int userId);
	public List<String> getAllUsers();

}
