package com.esprit.PI.services;

import java.util.List;

import com.esprit.PI.entities.Genre;
import com.esprit.PI.entities.Ouvrage;
import com.esprit.PI.entities.User;

public interface IOuvrageService {
	
	public int addOuvrage(Ouvrage ouvrage);
	public void deleteOuvrageById(int ouvrageId);
	public int getNombreOuvrage();
	public void updateOuvrageById(String titre,String genre,boolean emprunt,String ref,int ouvrageId);
	public List<String> getAllOuvrages();

}
