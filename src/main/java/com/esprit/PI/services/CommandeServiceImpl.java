package com.esprit.PI.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import com.esprit.PI.entities.Commande;
import com.esprit.PI.entities.Ouvrage;
import com.esprit.PI.repository.CommandeRepository;

@Service
@ComponentScan
@EnableJpaRepositories
public class CommandeServiceImpl implements ICommandeService {
	
	@Autowired
	CommandeRepository commandeRepository;
	

	@Override
	public int addCommande(Commande commande) {
		commandeRepository.save(commande);
		return commande.getId_commande();
	}
	


	@Override
	public void deleteCommandeById(int CommandeId) {
		Commande commande= commandeRepository.findById(CommandeId).get();
		commandeRepository.delete(commande);
		
	}

	@Override
	public int getNombreCommande() {
		return commandeRepository.getNombreCommande();
	}
	

	    //Incompatible types found: Iterable. Required: List
	    public List<Commande> findAll() {
	        return (List<Commande>) commandeRepository.findAll();
	    }



		@Override
		public List<String> getAllCommande() {
			return commandeRepository.getAllCommande();
		}



		@Override
		public void updateCommandeById(int CommandeId, boolean valide) {
			Commande commande = commandeRepository.findById(CommandeId).get();
			commandeRepository.updateCommandeById(CommandeId, valide);
			commandeRepository.save(commande);
			
		}

}
