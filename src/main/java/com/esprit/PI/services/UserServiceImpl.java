package com.esprit.PI.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.esprit.PI.entities.User;
import com.esprit.PI.repository.UserRepository;

@Service
@ComponentScan
@EnableJpaRepositories
public class UserServiceImpl implements IUserService {

	@Autowired
	 UserRepository userRepository;

	@Override
	public void deleteUserById(int userId) {
		User user = userRepository.findById(userId).get();
		userRepository.delete(user);
	}


	@Override
	public int getNombreUser() {
		return userRepository.getNombreUser();
	}

	@Override
	public void updateUserById(String nom_prenom, String email, boolean emprunt, int userId) {
		User user = userRepository.findById(userId).get();
		userRepository.updateUserById(nom_prenom, email, emprunt, userId);
		userRepository.save(user);
	}

	@Override
	public int addUser(User user) {
		userRepository.save(user);
		return user.getId_user();
	}

	@Override
	public List<String> getAllUsers() {
		return userRepository.getAllUsers();
	}

}
