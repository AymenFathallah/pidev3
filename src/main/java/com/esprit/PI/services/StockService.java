package com.esprit.PI.services;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import com.esprit.PI.entities.Livre;
import com.esprit.PI.entities.Ouvrage;

@Service
@ComponentScan
@EnableJpaRepositories
public interface StockService {
	
	public int ajoutStock(Ouvrage ouvrage);
	public void deleteStock(int ouvrageId);
//	public void updateOuvrageById(String titre,String genre,boolean emprunt,String ref,int ouvrageId);
//	public List<String> getAllOuvrages();
	public int rechercheStock(String titre);
}
