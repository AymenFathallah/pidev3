package com.esprit.PI.services;

import java.util.List;

import com.esprit.PI.entities.Offre;

public interface IOffreService {
	
	public int addOffre(Offre offre);
	public void AffectUserToOffre(int userId, int id_offre);
	public void deleteOffreById(int id_offre);
	public int getNombreOffre();
	public List<String> getAllOffre();
	public List<String> getAllOffreWithOuvrage(Integer ouvrage_id);
	public List<String> getOuvrageByOffre(Integer offre_id);
	public List<String> getAllOffreByUser(int id_user);
	public List<String> getOuvrageWithNoOffre();
	public List<String> getOuvrageWithOffre();

}
