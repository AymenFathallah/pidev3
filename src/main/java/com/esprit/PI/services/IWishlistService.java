package com.esprit.PI.services;

import java.util.List;

import com.esprit.PI.entities.Wishlist;

public interface IWishlistService {

	public int addWishlist(Wishlist wishlist);
	public void AffectUserToWishlist(int userId, int id_wishlist);
	public void deleteWishlistById(int id_wishlist);
	public int getNombreWishlist();
	public List<String> getAllWishlist();
	public List<String> getAllWishlistWithOuvrage(Integer ouvrage_id);
	public List<String> getOuvrageByWishlist(Integer wishlist_id);
	public List<String> getAllWishlistByUser(int id_user);
	public List<String> getOuvrageWithNoWishlist();
	public List<String> getOuvrageWithWishlist();

}
