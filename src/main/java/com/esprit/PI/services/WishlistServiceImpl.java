package com.esprit.PI.services;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import com.esprit.PI.entities.Ouvrage;
import com.esprit.PI.entities.Reclamation;
import com.esprit.PI.entities.User;
import com.esprit.PI.entities.Wishlist;
import com.esprit.PI.repository.OuvrageRepository;
import com.esprit.PI.repository.UserRepository;
import com.esprit.PI.repository.WishlistRepository;

@Service
@ComponentScan
@EnableJpaRepositories
public class WishlistServiceImpl implements IWishlistService{

	@Autowired
	WishlistRepository wishlistRepository;
	
	@Autowired
    OuvrageServiceImpl ouvrageService;
	
	@Autowired
	UserRepository userRepository;
	
	@Override
	public int addWishlist(Wishlist wishlist) {
		
		wishlist.setDate_creation(Calendar.getInstance().getTime());
		for(Ouvrage s : wishlist.getOuvrages()) {
			
			System.out.println(s.getId_ouvrage());
		}
		wishlistRepository.save(wishlist);
		return wishlist.getId_wishlist();

	}

	@Override
	public void deleteWishlistById(int id_wishlist) {
		Wishlist wishlist = wishlistRepository.findById(id_wishlist).get();
		wishlistRepository.delete(wishlist);
		
	}

	@Override
	public int getNombreWishlist() {
		return wishlistRepository.getNombreWishlist();
	}

	@Override
	public List<String> getAllWishlist() {
		return wishlistRepository.getAllWishlist();
	}

	@Override
	public List<String> getAllWishlistWithOuvrage(Integer ouvrage_id) {
		return wishlistRepository.getAllWishlistWithOuvrage(ouvrage_id);
	}

	@Override
	public void AffectUserToWishlist(int userId, int id_wishlist) { 
		User user = userRepository.findById(userId).get();
		Wishlist wish = wishlistRepository.findById(id_wishlist).get();
		wish.setUser(user);
		wishlistRepository.save(wish);
		
	}

	@Override
	public List<String> getOuvrageByWishlist(Integer wishlist_id) {
		return wishlistRepository.getOuvrageByWishlist(wishlist_id);
	}

	@Override
	public List<String> getAllWishlistByUser(int id_user) {
		return wishlistRepository.getAllWishlistByUser(id_user);
	}
	
	@Override
	public List<String> getOuvrageWithNoWishlist(){
		return wishlistRepository.getOuvrageWithNoWishlist();
	}
	
	@Override
	public List<String> getOuvrageWithWishlist(){
		return wishlistRepository.getOuvrageWithWishlist();
	}
	
}
