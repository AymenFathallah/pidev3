 package com.esprit.PI.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import com.esprit.PI.entities.Reclamation;
import com.esprit.PI.entities.Type;
import com.esprit.PI.entities.User;
import com.esprit.PI.repository.ReclamationRepository;
import com.esprit.PI.repository.UserRepository;



@Service
 @ComponentScan
 @EnableJpaRepositories
  
public class ReclamationServiceImpl implements IReclamationService {
	@Autowired
	 ReclamationRepository reclamationRepository;
	@Autowired
	UserRepository userRepository;
	@Override
	public int addReclamation(Reclamation reclamation) {
		reclamationRepository.save(reclamation);
		return reclamation.getId_reclamation();
	}

	@Override
	public void deleteReclamation(int reclamationId) {
		Reclamation rec = reclamationRepository.findById(reclamationId).get();
		reclamationRepository.delete(rec);
		
	}

	

	@Override
	public int getNombreReclamation() {
		return reclamationRepository.getNombreReclamation();

	}

	@Override
	public int getNombreReclamationTreated() {
		return reclamationRepository.getNombreReclamationTreated();
	}

	@Override
	public int getNombreReclamationNotTreated() {
		return reclamationRepository.getNombreReclamationNotTreated();
	}

	@Override
	public List<String> findAllReclamationByUser(int userId) {
		return reclamationRepository.findAllReclamationByUser(userId);
		
	}

	@Override
	public List<String> findAllReclamationTreatedByUser(int userId) {
		return reclamationRepository.findAllReclamationTreatedByUser(userId);
	}



	@Override
	public void AffectUserToReclamation(int userId, int reclamationId) { 
		User user = userRepository.findById(userId).get();
		Reclamation rec = reclamationRepository.findById(reclamationId).get();
		rec.setUser(user);
		reclamationRepository.save(rec);
		
	}

	@Override
	public List<String> findAllReclamation() {
		
		return reclamationRepository.findAllReclamation();
	}





	@Override
	public List<String> findAllReclamationTreatedByUserByType(int userId, Type typeRec) {
		return reclamationRepository.findAllReclamationTreatedByUserByType( userId,typeRec);

	}

	@Override
	public List<String> findAllReclamationSame(String objetRec, Type typeRec) {
		return reclamationRepository.findAllReclamationSame(objetRec, typeRec);
	}

	

}
