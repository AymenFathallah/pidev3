package com.esprit.PI.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import com.esprit.PI.entities.Ouvrage;
import com.esprit.PI.repository.OuvrageRepository;

@Service
@ComponentScan
@EnableJpaRepositories
public class OuvrageServiceImpl implements IOuvrageService {
	
	@Autowired
	 OuvrageRepository ouvrageRepository;

	@Override
	public void deleteOuvrageById(int ouvrageId) {
		Ouvrage ouvrage = ouvrageRepository.findById(ouvrageId).get();
		ouvrageRepository.delete(ouvrage);
	}


	@Override
	public int getNombreOuvrage() {
		return ouvrageRepository.getNombreOuvrage();
	}

	@Override
	public void updateOuvrageById(String titre, String genre, boolean emprunt,String ref, int ouvrageId) {
		Ouvrage ouvrage = ouvrageRepository.findById(ouvrageId).get();
		ouvrageRepository.updateOuvrageById(titre, genre, emprunt, ref, ouvrageId);
		ouvrageRepository.save(ouvrage);
	}

	@Override
	public int addOuvrage(Ouvrage ouvrage) {
		ouvrageRepository.save(ouvrage);
		return ouvrage.getId_ouvrage();
	}

	@Override
	public List<String> getAllOuvrages() {
		return ouvrageRepository.getAllOuvrages();
	}

}
