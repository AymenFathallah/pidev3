package com.esprit.PI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.PI.entities.Offre;
import com.esprit.PI.services.IOffreService;

@RestController
public class RestControllerOffre {
	
	@Autowired
	IOffreService offreService;
	String title="offre";
	
	@PostMapping(value = "/addoffre")
	@ResponseStatus(HttpStatus.CREATED)
		public int addOffre(@RequestBody Offre offre)
		{
			offreService.addOffre(offre);
			return offre.getId_offre();
		}
	
	 // URL : http://localhost:8081/SpringMVC/servlet/deleteoffre/1
    @DeleteMapping("/deleteoffre/{id_offre}") 
	@ResponseBody 
	public void deleteOffreById(@PathVariable("id_offre")int id_offre) {
    	offreService.deleteOffreById(id_offre);
		
	}
    
    // URL : http://localhost:8081/SpringMVC/servlet/getalloffre
    @GetMapping(value = "/getalloffre")
    @ResponseBody
	public List<String> getAllOffre() {
		
		return offreService.getAllOffre();
	}
    
    // url : http://localhost:8081/SpringMVC/servlet/getnombreoffre
    @GetMapping(value = "/getnombreoffre")
    @ResponseBody
    public int getNombreOffre() {
    	return offreService.getNombreOffre();
    }
    
	@RequestMapping("/hellooffre")
	public String sayHello() {
		System.out.println(title);
		return title;
	}
	
	// URL : http://localhost:8081/SpringMVC/servlet/getAllOffreWithOuvrage/2
    @GetMapping(value = "/getAllOffreWithOuvrage/{ouvrage_id}")
    @ResponseBody
	public List<String> getAllOffreWithOuvrage(@PathVariable("ouvrage_id")Integer ouvrage_id) {
		
		return offreService.getAllOffreWithOuvrage(ouvrage_id);
	}
    
 // http://localhost:8081/SpringMVC/servlet/AffectUserToOffre/4/4
 		@PutMapping(value = "/AffectUserToOffre/{userId}/{id_offre}") 
 		public void affectUserToOffre(@PathVariable("userId") int userId, @PathVariable("id_offre") int id_offre) {
 			 offreService.AffectUserToOffre(userId, id_offre);
 		}
 		
 	// URL : http://localhost:8081/SpringMVC/servlet/getOuvrageByOffre/2
 	    @GetMapping(value = "/getOuvrageByOffre/{offre_id}")
 	    @ResponseBody
 		public List<String> getOuvrageByOffre(@PathVariable("offre_id")Integer offre_id) {
 			
 			return offreService.getOuvrageByOffre(offre_id);
 		}
 	    
 	// URL : http://localhost:8081/SpringMVC/servlet/getAllOffreByUser/1
	    @GetMapping(value = "/getAllOffreByUser/{id_user}")
	    @ResponseBody
		public List<String> getAllOffreByUser(@PathVariable("id_user")int id_user) {
			
			return offreService.getAllOffreByUser(id_user);
		}
	    
	 // URL : http://localhost:8081/SpringMVC/servlet/getOuvrageWithNoOffre
	    @GetMapping(value = "/getOuvrageWithNoOffre")
	    @ResponseBody
		public List<String> getOuvrageWithNoOffre() {
			return offreService.getOuvrageWithNoOffre();
		}
	    
	 // URL : http://localhost:8081/SpringMVC/servlet/getOuvrageWithOffre
	    @GetMapping(value = "/getOuvrageWithOffre")
	    @ResponseBody
		public List<String> getOuvrageWithOffre() {
			return offreService.getOuvrageWithOffre();
		}

}
