package com.esprit.PI.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.PI.entities.Livre;
import com.esprit.PI.entities.Ouvrage;
import com.esprit.PI.entities.Reclamation;
import com.esprit.PI.services.IOuvrageService;
import com.esprit.PI.services.StockService;

@RestController
//@RequestMapping(value = "/api/stock")
public class StockController {
	
	String title="amineee";
	
	@Autowired
	StockService stockService;
	
	
	@PostMapping(value = "/addstock", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
		public Ouvrage addOuvrage(@RequestBody  Ouvrage ouvrage)
		{
		stockService.ajoutStock(ouvrage);
			return ouvrage;
		}
	
//	@GetMapping("/livre/list")
//	List<Livre> getAllLivre() throws IOException {		
//		return stockService.ajoutStock(ouvrage)();
//	}
//	
	
	
	
	
    // URL : http://localhost:8081/SpringMVC/servlet/getstock
    @GetMapping(value = "/getstock/{titre}")
    @ResponseBody
	public int getstock(@PathVariable("titre")String titre) {
		
		return stockService.rechercheStock(titre);
	}
	
	
	
	@RequestMapping("/hellostock")
	public String sayHello() {
		System.out.println(title);
		return title;
	}

}
