package com.esprit.PI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import com.esprit.PI.entities.Ouvrage;
import com.esprit.PI.services.IOuvrageService;

@Controller
@Component
@EnableJpaRepositories
public class IControllerOuvrage {
	
	@Autowired
	IOuvrageService iOuvrageService;

	public void deleteOuvrageById(int ouvrageId) {
		iOuvrageService.deleteOuvrageById(ouvrageId);
	}


	
	public int getNombreOuvrage() {
		return iOuvrageService.getNombreOuvrage();
	}

	
	public void updateOuvrageById(String titre, String genre, boolean emprunt, String ref, int ouvrageId) {
		iOuvrageService.updateOuvrageById(titre, genre, emprunt, ref, ouvrageId);	
	}


	public int addOuvrage(Ouvrage ouvrage) {
		iOuvrageService.addOuvrage(ouvrage);
		return ouvrage.getId_ouvrage();
	}

	
	public List<String> getAllOuvrages() {
		return iOuvrageService.getAllOuvrages();
	}
}
