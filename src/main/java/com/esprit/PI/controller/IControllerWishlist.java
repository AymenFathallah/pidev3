package com.esprit.PI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import com.esprit.PI.entities.Ouvrage;
import com.esprit.PI.entities.Wishlist;
import com.esprit.PI.services.IWishlistService;

@Controller
@Component
@EnableJpaRepositories
public class IControllerWishlist {

	@Autowired
	IWishlistService iWishlistService;

	public void deleteWishlistById(int id_wishlist) {
		iWishlistService.deleteWishlistById(id_wishlist);
	}

	public int getNombreWishlist() {
		return iWishlistService.getNombreWishlist();
	}

	public int addWishlist(Wishlist wishlist) {
		iWishlistService.addWishlist(wishlist);
		return wishlist.getId_wishlist();
	}

	public List<String> getAllWishlist() {
		return iWishlistService.getAllWishlist();
	}

	public List<String> getAllWishlistWithOuvrage(Integer ouvrage_id) {

		return iWishlistService.getAllWishlistWithOuvrage(ouvrage_id);
	}
	
	public void AffectUserToWishlist(int userId, int id_wishlist) {
 	   iWishlistService.AffectUserToWishlist(userId, id_wishlist);	
	}
	
	public List<String> getgetOuvrageByWishlist(Integer wishlist_id) {
		return iWishlistService.getOuvrageByWishlist(wishlist_id);
	}
	
	public List<String> getAllWishlistByUser(int id_user) {
		return iWishlistService.getAllWishlistByUser(id_user);
	}
	
	public List<String> getOuvrageWithNoWishlist() {
		return iWishlistService.getOuvrageWithNoWishlist();
	}
	
	public List<String> getOuvrageWithWishlist() {
		return iWishlistService.getOuvrageWithWishlist();
	}
}
