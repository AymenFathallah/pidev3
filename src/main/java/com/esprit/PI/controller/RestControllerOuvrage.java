package com.esprit.PI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.PI.entities.Ouvrage;
import com.esprit.PI.services.IOuvrageService;

@RestController
//@RequestMapping(value = "/api/ouvrage")
public class RestControllerOuvrage {

	@Autowired
	IOuvrageService ouvrageService;
	String title="Aymen";
	//{"titre":"khaled", "genre":"HISTORIQUE", "ref":"12L135"}
	
	@PostMapping(value = "/addouvrage", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
		public Ouvrage addOuvrage(@RequestBody Ouvrage ouvrage)
		{
			ouvrageService.addOuvrage(ouvrage);
			return ouvrage;
		}
	//URL http://localhost:8081/SpringMVC/servlet/updateouvrage/1/kk/BIOGRAPHY/1/000L111
	@PutMapping(value = "/updateouvrage/{ouvrageId}/{titre}/{genre}/{emprunt}/{ref}")
	@ResponseBody
		public void updateOuvrageById(@PathVariable("ouvrageId")int ouvrageId,@PathVariable("titre")String titre,@PathVariable("genre")String genre,@PathVariable("emprunt")boolean emprunt,@PathVariable("ref")String ref,@RequestBody Ouvrage ouvrage)
		{
		ouvrageService.updateOuvrageById(titre, genre, emprunt, ref, ouvrageId);
			 
		}
	
	 // URL : http://localhost:8081/SpringMVC/servlet/deleteouvrage/1
    @DeleteMapping("/deleteouvrage/{ouvrageId}") 
	@ResponseBody 
	public void deleteOuvrageById(@PathVariable("ouvrageId")int ouvrageId) {
    	ouvrageService.deleteOuvrageById(ouvrageId);
		
	}
    
    // URL : http://localhost:8081/SpringMVC/servlet/getallouvrages
    @GetMapping(value = "/getallouvrages")
    @ResponseBody
	public List<String> getAllOuvrages() {
		
		return ouvrageService.getAllOuvrages();
	}
    
    // url : http://localhost:8081/SpringMVC/servlet/getnombreouvrage
    @GetMapping(value = "/getnombreouvrage")
    @ResponseBody
    public int getNombreOuvrage() {
    	return ouvrageService.getNombreOuvrage();
    }
	@RequestMapping("/helloouvrage")
	public String sayHello() {
		System.out.println(title);
		return title;
	}
}
