package com.esprit.PI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import com.esprit.PI.entities.Reclamation;
import com.esprit.PI.entities.Type;
import com.esprit.PI.services.IReclamationService;


@Controller
@Component
@EnableJpaRepositories
public class IControllerReclamationImpl {
	@Autowired
	IReclamationService irecservice;
	
	
	public int addReclamation(Reclamation reclamation) {
		irecservice.addReclamation(reclamation);
		return reclamation.getId_reclamation();
	}
	
	public void deleteReclamation(int reclamationId) {
		irecservice.deleteReclamation(reclamationId);
		
	}

	
	public int getNombreReclamation() {
		return irecservice.getNombreReclamation();

	}

	
	public int getNombreReclamationTreated() {
		return irecservice.getNombreReclamationTreated();
	}

	
	public int getNombreReclamationNotTreated() {
		return irecservice.getNombreReclamationNotTreated();
	}


	public List<String> findAllReclamationByUser(int userId) {
		return irecservice.findAllReclamationByUser(userId);
	}

       public List<String> findAllReclamation() {
		
		return irecservice.findAllReclamation();
	}

       public void AffectUserToReclamation(int userId, int reclamationId) {
    	   irecservice.AffectUserToReclamation(userId, reclamationId);	
   	}

	public List<String> findAllReclamationTreatedByUser(int userId) {
		
		return  irecservice.findAllReclamationTreatedByUser(userId);
	}

	
	public List<String> findAllReclamationTreatedByUserByType(int userId, Type typeRec) {
		return irecservice.findAllReclamationTreatedByUserByType(userId, typeRec);
	}

	public List<String> findAllReclamationSame(String objetRec, Type typeRec) {
		return irecservice.findAllReclamationSame(objetRec, typeRec);
	}

	

}
