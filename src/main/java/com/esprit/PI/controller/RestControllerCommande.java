package com.esprit.PI.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.PI.entities.Commande;
import com.esprit.PI.entities.Ouvrage;
import com.esprit.PI.services.ICommandeService;
import com.esprit.PI.services.IOuvrageService;

@RestController
//@RequestMapping(value = "/api/commande")
public class RestControllerCommande {
	
	@Autowired
	ICommandeService commandeService;

	
	
	@PostMapping(value = "/addcommande", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
		public Commande addOCommande(@RequestBody Commande commande)
		{
		commandeService.addCommande(commande);
			return commande;
		}
	
    // URL : http://localhost:8081/SpringMVC/servlet/getallcommandes
    @GetMapping(value = "/getallcommandes")
    @ResponseBody
	public List<String> getAllCommandes() { 	
    	return commandeService.getAllCommande();
	}

    
    // url : http://localhost:8081/SpringMVC/servlet/getnombrecommande
    @GetMapping(value = "/getnombrecommande")
    @ResponseBody
    public int getNombreOuvrage() {
    	return commandeService.getNombreCommande();
    }
	@RequestMapping("/hellcommandeoouvrage")
	public LocalDateTime sayHello() {
		//return title;
		return LocalDateTime.now() ;
	}
	
	//URL http://localhost:8081/SpringMVC/servlet/updatecommande/
	@PutMapping(value = "/updatecommande/{CommandeId}/{valide}")
	@ResponseBody
		public void updateCommandeById(@PathVariable("valide")boolean valide,@PathVariable("CommandeId")int CommandeId)
		{
		commandeService.updateCommandeById(CommandeId, valide);
			
		}
	
	 // URL : http://localhost:8081/SpringMVC/servlet/deletecommande/
    @DeleteMapping("/deletecommande/{commandeId}") 
	@ResponseBody 
	public void deleteOuvrageById(@PathVariable("commandeId")int commandeId) {
    	commandeService.deleteCommandeById(commandeId);
		
	}

}
