package com.esprit.PI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.PI.entities.Reclamation;
import com.esprit.PI.entities.Type;
import com.esprit.PI.services.IReclamationService;

@RestController
public class RestControlReclamation {
	@Autowired
	IReclamationService reclamationservice;
	String title="abir";
	//{"id_reclamation":1,"objet":"cc", "contenu":"hello", "treated":"false", "id_user":"4" , "type" :"SERVICE"}
	
		@PostMapping(value = "/addrec", produces = MediaType.APPLICATION_JSON_VALUE)
		@ResponseStatus(HttpStatus.CREATED)
			public Reclamation addReclamation(@RequestBody  Reclamation rec)
			{
			reclamationservice.addReclamation(rec);
				return rec;
			}
		
		// URL : http://localhost:8081/SpringMVC/servlet/deleterecById/1
	    @DeleteMapping("/deleterecById/{reclamationId}") 
		@ResponseBody 
		public void deleteReclamationById(@PathVariable("reclamationId")int reclamationId) {
	    	reclamationservice.deleteReclamation(reclamationId);
			
		}
	    // URL : http://localhost:8081/SpringMVC/servlet/getAllUsers
	    @GetMapping(value = "/getNombreReclamation")
	    @ResponseBody
		public int getNombreReclamation() {
			
			return reclamationservice.getNombreReclamation();
		}

	    // URL : http://localhost:8081/SpringMVC/servlet/getNombreReclamationTreated
	    @GetMapping(value = "/getNombreReclamationTreated")
	    @ResponseBody
		public int getNombreReclamationTreated() {
			
			return reclamationservice.getNombreReclamationTreated();
		}
	    
	    // URL : http://localhost:8081/SpringMVC/servlet/getNombreReclamationNotTreated
	    @GetMapping(value = "/getNombreReclamationNotTreated")
	    @ResponseBody
		public int getNombreReclamationNotTreated() {
			
			return reclamationservice.getNombreReclamationNotTreated();
		}
	    
	 // URL : http://localhost:8081/SpringMVC/servlet/getAllReclamationByUser/1
	    @GetMapping(value = "/getAllReclamationByUser/{reclamationId}")
	    @ResponseBody
		public List<String> findAllReclamationByUser(@PathVariable("reclamationId")int reclamationId) {
			
			return reclamationservice.findAllReclamationByUser(reclamationId);
		}
	    
	 // URL : http://localhost:8081/SpringMVC/servlet/getAllReclamationTreatedByUser/4
	    @GetMapping(value = "/getAllReclamationTreatedByUser/{userId}")
	    @ResponseBody
		public List<String> findAllReclamationTreatedByUser(@PathVariable("userId")int userId) {
			
			return reclamationservice.findAllReclamationTreatedByUser(userId);
		}
	    
	    
	    // URL : http://localhost:8081/SpringMVC/servlet/getAllReclamationTreatedByUserByType/4/SERVICE
	    @GetMapping(value = "/getAllReclamationTreatedByUserByType/{userId}/{typeRec}")
	    @ResponseBody
		public List<String> findAllReclamationTreatedByUserByType(@PathVariable("userId")int userId,@PathVariable("typeRec")Type typeRec) {
			
			return reclamationservice.findAllReclamationTreatedByUserByType(userId, typeRec);
		}
	    
	    // URL : http://localhost:8081/SpringMVC/servlet/getAllReclamationTreatedByUserByType/4/SERVICE
	    @GetMapping(value = "/findAllReclamationSame/{objetRec}/{typeRec}")
	    @ResponseBody
		public List<String> findAllReclamationSame(@PathVariable("objetRec")String objetRec,@PathVariable("typeRec")Type typeRec) {
			
			return reclamationservice.findAllReclamationSame(objetRec, typeRec);
		}
	    
	 // URL : http://localhost:8081/SpringMVC/servlet/getAllReclamation
	    @GetMapping(value = "/getAllReclamation")
	    @ResponseBody
		public List<String> findAllReclamation() {
			
			return reclamationservice.findAllReclamation();
		}
	 // http://localhost:8081/SpringMVC/servlet/AffectUserToReclamation/4/4
		@PutMapping(value = "/AffectUserToReclamation/{userId}/{reclamationId}") 
		public void affecterMissionADepartement(@PathVariable("userId") int userId, @PathVariable("reclamationId") int reclamationId) {
			 reclamationservice.AffectUserToReclamation(userId, reclamationId);

		}
		@RequestMapping("/helloRec")
		public String sayHello() {
			System.out.println(title);
			return title;
		}
}
