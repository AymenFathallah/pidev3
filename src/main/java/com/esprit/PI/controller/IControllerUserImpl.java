package com.esprit.PI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import com.esprit.PI.entities.User;
import com.esprit.PI.services.IUserService;

@Controller
@Component
@EnableJpaRepositories
public class IControllerUserImpl {
	@Autowired
	IUserService iuserservice;
	
	public void deleteUserById(int userId) {
		iuserservice.deleteUserById(userId);
	}


	
	public int getNombreUser() {
		return iuserservice.getNombreUser();
	}

	
	public void updateUserById(String nom_prenom, String email, boolean emprunt, int userId) {
		iuserservice.updateUserById(nom_prenom, email, emprunt, userId);	
	}


	public int addUser(User user) {
		iuserservice.addUser(user);
		return user.getId_user();
	}

	
	public List<String> getAllUsers() {
		return iuserservice.getAllUsers();
	}
}
