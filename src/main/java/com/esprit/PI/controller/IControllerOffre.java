package com.esprit.PI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import com.esprit.PI.entities.Offre;
import com.esprit.PI.services.IOffreService;

@Controller
@Component
@EnableJpaRepositories
public class IControllerOffre {

	@Autowired
	IOffreService iOffreService;

	public void deleteOffreById(int id_offre) {
		iOffreService.deleteOffreById(id_offre);
	}

	public int getNombreOffre() {
		return iOffreService.getNombreOffre();
	}

	public int addOffre(Offre offre) {
		iOffreService.addOffre(offre);
		return offre.getId_offre();
	}

	public List<String> getAllOffre() {
		return iOffreService.getAllOffre();
	}

	public List<String> getAllOffreWithOuvrage(Integer ouvrage_id) {

		return iOffreService.getAllOffreWithOuvrage(ouvrage_id);
	}
	
	public void AffectUserToOffre(int userId, int id_offre) {
 	   iOffreService.AffectUserToOffre(userId, id_offre);	
	}
	
	public List<String> getgetOuvrageByOffre(Integer offre_id) {
		return iOffreService.getOuvrageByOffre(offre_id);
	}
	
	public List<String> getAllOffreByUser(int id_user) {
		return iOffreService.getAllOffreByUser(id_user);
	}
	
	public List<String> getOuvrageWithNoOffre() {
		return iOffreService.getOuvrageWithNoOffre();
	}
	
	public List<String> getOuvrageWithOffre() {
		return iOffreService.getOuvrageWithOffre();
	}
}
