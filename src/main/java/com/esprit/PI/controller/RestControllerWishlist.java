package com.esprit.PI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.PI.entities.Wishlist;
import com.esprit.PI.services.IWishlistService;

@RestController
//@RequestMapping("/wish")
public class RestControllerWishlist {
	
	@Autowired
	IWishlistService wishlistService;
	String title="wishlist";
	
	@PostMapping(value = "/addwishlist")
	@ResponseStatus(HttpStatus.CREATED)
		public int addWishlist(@RequestBody Wishlist wishlist)
		{
			wishlistService.addWishlist(wishlist);
			return wishlist.getId_wishlist();
		}
	
	 // URL : http://localhost:8081/SpringMVC/servlet/deletewishlist/1
    @DeleteMapping("/deletewishlist/{id_wishlist}") 
	@ResponseBody 
	public void deleteWishlistById(@PathVariable("id_wishlist")int id_wishlist) {
    	wishlistService.deleteWishlistById(id_wishlist);
		
	}
    
    // URL : http://localhost:8081/SpringMVC/servlet/getallwishlist
    @GetMapping(value = "/getallwishlist")
    @ResponseBody
	public List<String> getAllWishlist() {
		
		return wishlistService.getAllWishlist();
	}
    
    // url : http://localhost:8081/SpringMVC/servlet/getnombrewishlist
    @GetMapping(value = "/getnombrewishlist")
    @ResponseBody
    public int getNombreWishlist() {
    	return wishlistService.getNombreWishlist();
    }
    
	@RequestMapping("/hellowishlist")
	public String sayHello() {
		System.out.println(title);
		return title;
	}
	
	// URL : http://localhost:8081/SpringMVC/servlet/getAllWishlistWithOuvrage/2
    @GetMapping(value = "/getAllWishlistWithOuvrage/{ouvrage_id}")
    @ResponseBody
	public List<String> getAllWishlistWithOuvrage(@PathVariable("ouvrage_id")Integer ouvrage_id) {
		
		return wishlistService.getAllWishlistWithOuvrage(ouvrage_id);
	}
    
 // http://localhost:8081/SpringMVC/servlet/AffectUserToWishlist/4/4
 		@PutMapping(value = "/AffectUserToWishlist/{userId}/{id_wishlist}") 
 		public void affectUserToWishlist(@PathVariable("userId") int userId, @PathVariable("id_wishlist") int id_wishlist) {
 			 wishlistService.AffectUserToWishlist(userId, id_wishlist);
 		}
 		
 	// URL : http://localhost:8081/SpringMVC/servlet/getOuvrageByWishlist/2
 	    @GetMapping(value = "/getOuvrageByWishlist/{wishlist_id}")
 	    @ResponseBody
 		public List<String> getOuvrageByWishlist(@PathVariable("wishlist_id")Integer wishlist_id) {
 			
 			return wishlistService.getOuvrageByWishlist(wishlist_id);
 		}
 	    
 	// URL : http://localhost:8081/SpringMVC/servlet/getAllWishlistByUser/1
	    @GetMapping(value = "/getAllWishlistByUser/{id_user}")
	    @ResponseBody
		public List<String> getAllWishlistByUser(@PathVariable("id_user")int id_user) {
			
			return wishlistService.getAllWishlistByUser(id_user);
		}
	    
	 // URL : http://localhost:8081/SpringMVC/servlet/getOuvrageWithNoWishlist
	    @GetMapping(value = "/getOuvrageWithNoWishlist")
	    @ResponseBody
		public List<String> getOuvrageWithNoWishlist() {
			return wishlistService.getOuvrageWithNoWishlist();
		}
	    
	 // URL : http://localhost:8081/SpringMVC/servlet/getOuvrageWithWishlist
	    @GetMapping(value = "/getOuvrageWithWishlist")
	    @ResponseBody
		public List<String> getOuvrageWithWishlist() {
			return wishlistService.getOuvrageWithWishlist();
		}
}
