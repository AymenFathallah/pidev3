package com.esprit.PI.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.PI.entities.User;
import com.esprit.PI.services.IUserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
@RestController
//@RequestMapping(value = "/api/user")
public class RestControlUser {
	@Autowired
	IUserService userservice;
	String title="abir";
	//{"id_user":4,"nom_prenom":"khaled", "cin":"01235698", "email":"test@hm", "role":"ADMINISTRATEUR"}
	
	@PostMapping(value = "/adduser", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
		public User addUser(@RequestBody User user)
		{
			userservice.addUser(user);
			return user;
		}
	//URL http://localhost:8081/SpringMVC/servlet/Updateuser/4/kk/ll/1
	@PutMapping(value = "/Updateuser/{userId}/{nom_prenom}/{email}/{emprunt}")
	@ResponseBody
		public void updateUserById(@PathVariable("userId")int userId,@PathVariable("nom_prenom")String nom_prenom,@PathVariable("email")String email,@PathVariable("emprunt")boolean emprunt,@RequestBody User user)
		{
		userservice.updateUserById(nom_prenom, email, emprunt, userId);
			
		}
	
	 // URL : http://localhost:8081/SpringMVC/servlet/deleteUserById/1
    @DeleteMapping("/deleteUserById/{userId}") 
	@ResponseBody 
	public void deleteEmployeById(@PathVariable("userId")int userId) {
    	userservice.deleteUserById(userId);
		
	}
    
    // URL : http://localhost:8081/SpringMVC/servlet/getAllUsers
    @GetMapping(value = "/getAllUsers")
    @ResponseBody
	public List<String> getAllUsers() {
		
		return userservice.getAllUsers();
	}
    
    // url : http://localhost:8081/SpringMVC/servlet/getNombreUser
    @GetMapping(value = "/getNombreUser")
    @ResponseBody
    public int getNombreUser() {
    	return userservice.getNombreUser();
    }
	@RequestMapping("/hello")
	public String sayHello() {
		System.out.println(title);
		return title;
	}
		
}
