package com.esprit.PI.controller;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SendMail {
	
//	   @RequestMapping(value = "/sendemail")
//	   public String sendEmail() {
//	      return "Email sent successfully";
//	   }  
//	   
	@Autowired
    private JavaMailSender javaMailSender;
	
	@RequestMapping("/sendmail")
	 void sendEmailWithAttachment() throws MessagingException, IOException {

	        MimeMessage msg = javaMailSender.createMimeMessage();

	        // true = multipart message
	        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
	        
	        helper.setTo("mohamedamine.benhassine@esprit.tn");

	        helper.setSubject("Testing from Spring Boot");

	        // default = text/plain
	        //helper.setText("Check attachment for image!");

	        // true = text/html
	        helper.setText("<h2>Bonjour  Mr/Mme votre commande a été saisi avec succés!</h2>", true);

	        // hard coded a file path
	        //FileSystemResource file = new FileSystemResource(new File("path/android.png"));

	      //  helper.addAttachment("my_photo.png", new ClassPathResource("android.png"));

	        javaMailSender.send(msg);

	    }
	   
	}

