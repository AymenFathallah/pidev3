package com.esprit.PI.entities;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

@Entity
public class Reclamation implements Serializable {

	private static final long serialVersionUID = -1396669830860400871L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_reclamation;
	private String objet;
	private String contenu;
	private boolean treated;
	@Enumerated(EnumType.STRING)
	@NotNull
	private Type type;
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@JsonIgnore
	@ManyToOne @JoinColumn(name="id_user",referencedColumnName = "id_user", insertable=false, updatable=true)
    private User user;
	
	
	public Reclamation() {
		super();
	}
	public Reclamation(int id_reclamation, String objet, String contenu, boolean treated, Type type, User user) {
		super();
		this.id_reclamation = id_reclamation;
		this.objet = objet;
		this.contenu = contenu;
		this.treated = treated;
		this.type = type;
		this.user = user;
	}
	public int getId_reclamation() {
		return id_reclamation;
	}

	public void setId_reclamation(int id_reclamation) {
		this.id_reclamation = id_reclamation;
	}

	public String getObjet() {
		return objet;
	}

	public void setObjet(String objet) {
		this.objet = objet;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public boolean isTreated() {
		return treated;
	}

	public void setTreated(boolean treated) {
		this.treated = treated;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}
