package com.esprit.PI.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.sun.istack.NotNull;

@Entity
public class Ouvrage implements Serializable{

	
	private static final long serialVersionUID = -3139310478691527045L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_ouvrage;
	
	private String titre;
	
	@Enumerated(EnumType.STRING)
	@NotNull
	private Genre genre;
	
	private String ref;
	
	private int nb_emprunt;
	
	private boolean emprunt;
	
	private int stock;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "ouvrages")
	private Set<Wishlist> wishlists = new HashSet<>();

	public Ouvrage() {
		super();
	}



	public Ouvrage(int id_ouvrage, String titre, Genre genre, String ref, int nb_emprunt, boolean emprunt, int stock) {
		super();
		this.id_ouvrage = id_ouvrage;
		this.titre = titre;
		this.genre = genre;
		this.ref = ref;
		this.nb_emprunt = nb_emprunt;
		this.emprunt = emprunt;
		this.stock = stock;
	}



	@Override
	public String toString() {
		return "Ouvrage [id_ouvrage=" + id_ouvrage + ", titre=" + titre + ", genre=" + genre + ", ref=" + ref
				+ ", nb_emprunt=" + nb_emprunt + ", emprunt=" + emprunt + ", stock=" + stock + "]";
	}



	public int getId_ouvrage() {
		return id_ouvrage;
	}



	public void setId_ouvrage(int id_ouvrage) {
		this.id_ouvrage = id_ouvrage;
	}



	public String getTitre() {
		return titre;
	}



	public void setTitre(String titre) {
		this.titre = titre;
	}



	public Genre getGenre() {
		return genre;
	}



	public void setGenre(Genre genre) {
		this.genre = genre;
	}



	public String getRef() {
		return ref;
	}



	public void setRef(String ref) {
		this.ref = ref;
	}



	public int getNbEmprunt() {
		return nb_emprunt;
	}



	public void setNbEmprunt(int nbEmprunt) {
		this.nb_emprunt = nbEmprunt;
	}



	public boolean isEmprunt() {
		return emprunt;
	}



	public void setEmprunt(boolean emprunt) {
		this.emprunt = emprunt;
	}



	public int getStock() {
		return stock;
	}



	public void setStock(int stock) {
		this.stock = stock;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public Set<Wishlist> getWishlists() {
		return wishlists;
	}



	public void setWishlists(Set<Wishlist> wishlists) {
		this.wishlists = wishlists;
	}
	
	
	
	

}
