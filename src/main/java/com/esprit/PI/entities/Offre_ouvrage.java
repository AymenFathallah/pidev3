package com.esprit.PI.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "offre_ouvrage")
public class Offre_ouvrage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4950867493507713119L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_offre_ouvrage;
	
	private int offre_id;
	private Integer ouvrage_id;
	public int getId_offre_ouvrage() {
		return id_offre_ouvrage;
	}
	public void setId_offre_ouvrage(int id_offre_ouvrage) {
		this.id_offre_ouvrage = id_offre_ouvrage;
	}
	public int getOffre_id() {
		return offre_id;
	}
	public void setOffre_id(int offre_id) {
		this.offre_id = offre_id;
	}
	public Integer getOuvrage_id() {
		return ouvrage_id;
	}
	public void setOuvrage_id(Integer ouvrage_id) {
		this.ouvrage_id = ouvrage_id;
	}
	
	
}
