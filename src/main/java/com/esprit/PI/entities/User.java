package com.esprit.PI.entities;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

@Entity
public class User implements Serializable {

	private static final long serialVersionUID = -1396669830860400871L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_user;
	
	private String nom_prenom;
	
	@Column(unique=true)
	private long cin;
	
	
	private String email;

	private Date date_naissance;
	private Date date_inscri;

	private boolean emprunt;

	@Enumerated(EnumType.STRING)
	@NotNull
	private Role role;
	
	@Override
	public String toString() {
		return "User [id_user=" + id_user + ", nom_prenom=" + nom_prenom + ", cin=" + cin + ", email=" + email
				+ ", date_naissance=" + date_naissance + ", date_inscri=" + date_inscri + ", emprunt=" + emprunt
				+ ", role=" + role + "]";
	}

	public User() {
		super();
		
	}

	public User(int id_user, String nom_prenom, long cin, String email, Date date_naissance, Date date_inscri,
			boolean emprunt, Role role) {
		super();
		this.id_user = id_user;
		this.nom_prenom = nom_prenom;
		this.cin = cin;
		this.email = email;
		this.date_naissance = date_naissance;
		this.date_inscri = date_inscri;
		this.emprunt = emprunt;
		this.role = role;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	public String getNom_prenom() {
		return nom_prenom;
	}

	public void setNom_prenom(String nom_prenom) {
		this.nom_prenom = nom_prenom;
	}

	public long getCin() {
		return cin;
	}

	public void setCin(long cin) {
		this.cin = cin;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDate_naissance() {
		return date_naissance;
	}

	public void setDate_naissance(Date date_naissance) {
		this.date_naissance = date_naissance;
	}

	public Date getDate_inscri() {
		return date_inscri;
	}

	public void setDate_inscri(Date date_inscri) {
		this.date_inscri = date_inscri;
	}

	public boolean isEmprunt() {
		return emprunt;
	}

	public void setEmprunt(boolean emprunt) {
		this.emprunt = emprunt;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	
	
	
	
	
}


