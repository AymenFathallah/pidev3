package com.esprit.PI.entities;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;


@Entity
public class Commande {
	
	@Id @GeneratedValue
	private int id_commande;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@NotNull
	private LocalDate dateCommande = LocalDate.now();
	
	@NotNull
	private boolean valide;
	
	@JsonIgnore
	@ManyToOne @JoinColumn(name="id_user",referencedColumnName = "id_user", insertable=false, updatable=true)
	private User client;
	
	@JsonIgnore
	@ManyToOne @JoinColumn(name="id_ouvrage",referencedColumnName = "id_ouvrage", insertable=false, updatable=true)

	 private   Ouvrage ouvrage;

	@Override
	public String toString() {
		return "Commande [id_commande=" + id_commande + ", dateCommande=" + dateCommande + ", valide=" + valide
				+ ", client=" + client + "]";
	}









	public boolean isValide() {
		return valide;
	}




	public void setValide(boolean valide) {
		this.valide = valide;
	}




	public User getClient() {
		return client;
	}




	public void setClient(User client) {
		this.client = client;
	}


	public Commande(int id_commande, LocalDate dateCommande, boolean valide, User client, List<Ouvrage> lignesCommande) {
		super();
		this.id_commande = id_commande;
		this.dateCommande = LocalDate.now();
		this.valide = valide;
		this.client = client;
	}


	public int getId_commande() {
		return id_commande;
	}



	public Commande() {}




	public LocalDate getDateCommande() {
		return dateCommande;
	}


	public void setDateCommande(LocalDate dateCommande) {
		this.dateCommande = dateCommande;
	}

}
