package com.esprit.PI.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Wishlist")
public class Wishlist implements Serializable {

	private static final long serialVersionUID = -6612830550483963342L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_wishlist;
	
	private String nom_wishlist;
	private String description;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "wishlist_ouvrage",
		joinColumns = { @JoinColumn(name = "wishlist_id")},
		inverseJoinColumns = { @JoinColumn (name = "ouvrage_id")})
	private Set<Ouvrage> ouvrages = new HashSet<>();
	
	private Date date_creation;
	
	@JsonIgnore
	@OneToOne @JoinColumn(name="id_user",referencedColumnName = "id_user", insertable=false, updatable=true)
    private User user;
	

	public int getId_wishlist() {
		return id_wishlist;
	}

	public void setId_wishlist(int id_wishlist) {
		this.id_wishlist = id_wishlist;
	}

	public Date getDate_creation() {
		return date_creation;
	}

	public void setDate_creation(Date date_creation) {
		this.date_creation = date_creation;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User id_user) {
		this.user = id_user;
	}

	public String getNom_wishlist() {
		return nom_wishlist;
	}

	public void setNom_wishlist(String nom_wishlist) {
		this.nom_wishlist = nom_wishlist;
	}

	public Wishlist() {
		super();
	}

	public Set<Ouvrage> getOuvrages() {
		return ouvrages;
	}

	public void setOuvrages(Set<Ouvrage> ouvrages) {
		this.ouvrages = ouvrages;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Wishlist [id_wishlist=" + id_wishlist + ", nom_wishlist=" + nom_wishlist + ", ouvrages=" + ouvrages
				+ ", date_creation=" + date_creation + ", user=" + user + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date_creation == null) ? 0 : date_creation.hashCode());
		result = prime * result + id_wishlist;
		result = prime * result + ((nom_wishlist == null) ? 0 : nom_wishlist.hashCode());
		result = prime * result + ((ouvrages == null) ? 0 : ouvrages.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Wishlist other = (Wishlist) obj;
		if (date_creation == null) {
			if (other.date_creation != null)
				return false;
		} else if (!date_creation.equals(other.date_creation))
			return false;
		if (id_wishlist != other.id_wishlist)
			return false;
		if (nom_wishlist == null) {
			if (other.nom_wishlist != null)
				return false;
		} else if (!nom_wishlist.equals(other.nom_wishlist))
			return false;
		if (ouvrages == null) {
			if (other.ouvrages != null)
				return false;
		} else if (!ouvrages.equals(other.ouvrages))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	
}

