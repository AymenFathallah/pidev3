package com.esprit.PI.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Offre")
public class Offre implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4595483958405332123L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_offre;
	
	private String nom_offre;
	private String description;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "offre_ouvrage",
		joinColumns = { @JoinColumn(name = "offre_id")},
		inverseJoinColumns = { @JoinColumn (name = "ouvrage_id")})
	private Set<Ouvrage> ouvrages = new HashSet<>();
	
	private String pourcentage;
	
	@JsonIgnore
	@OneToOne @JoinColumn(name="id_user",referencedColumnName = "id_user", insertable=false, updatable=true)
    private User user;

	public int getId_offre() {
		return id_offre;
	}

	public void setId_offre(int id_offre) {
		this.id_offre = id_offre;
	}

	public String getNom_offre() {
		return nom_offre;
	}

	public void setNom_offre(String nom_offre) {
		this.nom_offre = nom_offre;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Ouvrage> getOuvrages() {
		return ouvrages;
	}

	public void setOuvrages(Set<Ouvrage> ouvrages) {
		this.ouvrages = ouvrages;
	}

	public String getPourcentage() {
		return pourcentage;
	}

	public void setPourcentage(String pourcentage) {
		this.pourcentage = pourcentage;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Offre [id_offre=" + id_offre + ", nom_offre=" + nom_offre + ", description=" + description
				+ ", ouvrages=" + ouvrages + ", pourcentage=" + pourcentage + ", user=" + user + "]";
	}

	public Offre() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id_offre;
		result = prime * result + ((nom_offre == null) ? 0 : nom_offre.hashCode());
		result = prime * result + ((ouvrages == null) ? 0 : ouvrages.hashCode());
		result = prime * result + ((pourcentage == null) ? 0 : pourcentage.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Offre other = (Offre) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id_offre != other.id_offre)
			return false;
		if (nom_offre == null) {
			if (other.nom_offre != null)
				return false;
		} else if (!nom_offre.equals(other.nom_offre))
			return false;
		if (ouvrages == null) {
			if (other.ouvrages != null)
				return false;
		} else if (!ouvrages.equals(other.ouvrages))
			return false;
		if (pourcentage == null) {
			if (other.pourcentage != null)
				return false;
		} else if (!pourcentage.equals(other.pourcentage))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	} 
	
	
}
