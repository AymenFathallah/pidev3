package com.esprit.PI.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "wishlist_ouvrage")
public class Wishlist_ouvrage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5475493554295646343L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_wishlist_ouvrage;
	
	private int wishlist_id;
	private Integer ouvrage_id;
	
	public Integer getWishlist_id() {
		return wishlist_id;
	}
	public void setWishlist_id(Integer wishlist_id) {
		this.wishlist_id = wishlist_id;
	}
	public int getOuvrage_id() {
		return ouvrage_id;
	}
	public void setOuvrage_id(int ouvrage_id) {
		this.ouvrage_id = ouvrage_id;
	}
	
	
}
